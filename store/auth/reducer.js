const INITIAL_STATE = {isAuthenticated: false, user: {}};

export default function userReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'REQ_LOGIN_SUCCESS':
            return {
                ...state,
                ...action.payload,
                isLoading: false,
                isAuthenticated: true,
            };

        case 'REQ_LOGOUT':
            return {
                INITIAL_STATE
            };
        case 'REQ_LOGIN_GOOGLE':
            return {...state, errorMessage: '', isError: false, isLoading: true};
        case 'REQ_LOGIN_GOOGLE_SUCCESS':
            return {
                ...state,
                ...action.payload,
                isLoading: false,
                isAuthenticated: true,
            };
        case 'REQ_LOGIN_GOOGLE_FAILED':
            return {
                ...state,
                isError: true,
                isAuthenticated: false,
                isLoading: false,
                errorMessage: action.payload,
            };
        case 'GET_ADDRESS':
            return {...state, errorMessage: '', isError: false, isLoading: true};
        case 'GET_ADDRESS_SUCCESS':
            return {
                ...state,
                address: action.payload.addressList,
                addressDefault: action.payload.addressActive,
                isLoading: false,
                isAuthenticated: true,
            };
        case 'GET_ADDRESS_FAILED':
            return {
                ...state,
                isError: true,
                isAuthenticated: false,
                isLoading: false,
                errorMessage: action.payload,
            };
        default:
            return state;
    }
}