import {combineReducers} from 'redux';
import configureStore from './CreateStore';

import authReducer from './auth/reducer';

const rootReducer = combineReducers({
    auth: authReducer,
    
});

const store = configureStore(rootReducer);

export default store;