import axios from 'axios';
import react from 'react'
// import {ToastAndroid} from 'react-native'
// import redux from '../stores';
// import Config from './Config'

// const {store} = redux;

// store.subscribe(listener);

let urlDev = 'https://rajarental-dev.sternproject.com/api/v1.0/app'
let urlProd = 'https://api.rajarental.co.id/api/v1.0/app'

const Api = axios.create({
  baseURL: urlProd,
  timeout: 30000,
});

// function selectToken(state) {
//   return state?.user?.token;
// }

// function listener() {
//   const token = selectToken(store.getState());
//   if (token) {
//     axios.defaults.headers.common["token"] = token;
//     Api.defaults.headers.common["token"] = token;
//   }
// }

Api.interceptors.response.use(
  (response) => {
    // if (response.data.errorCode === 11003
    //   // response.data.errorCode === 11002 ||
    //   // response.data.errorCode === 10006
    //   ) {
    //     console.log(response.data, 'errorrrr')
    //     store.dispatch({
    //       type: 'REQ_LOGOUT'
    //     })
    // }

    return response;
  },
  function (error) {
    if (error?.message === "Network Error") {
      ToastAndroid.show("Server can't be reach",ToastAndroid.LONG,  ToastAndroid.CENTER);
    }
    return Promise.reject(error);
  }


);


export default Api;
