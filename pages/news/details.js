import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import moment from 'moment';
import 'moment/locale/id'

import styles from '../../styles/Home.module.css'
import Api from '../../utils/Api'
import Navbar from '../../component/navbar'
import Footer from '../../component/footer'
import WaFloat from '../../component/wa-float'
moment.locale('id')

export default function Home() {

    const [detail, setdetail] = useState({})
    const [latest, setlatest] = useState([])

    const router = useRouter()
    const {slug} = router.query
    
    useEffect(() => {
        getnews()
        getlatest()
    }, [])

    const getnews= async () => {
        try {
          let param = router.asPath?.split("?")?.[1]?.split("=")
          let fetch = await Api.get('/article/news/' + param[1])
    
          if(fetch) {
            console.log(fetch.data, 'news')
            setdetail({...fetch.data.response, tags: fetch.data.response.tags.split(",")})
          }
        } catch (error) {
          
        }
    }

    const  getlatest = async () => {
        try {
            let fetch = await Api.get('/article/news', {
                params: {
                    pagination: true,
                    page: 1,
                    row: 5,
                    order_by: 'created_by',
                    order_type: 'DESC'
                }
            })
      
            if(fetch) {
              console.log(fetch.data, 'news')
              setlatest(fetch.data.response.rows)
            }
          } catch (error) {
            
          }
      }

    let arr = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    return (
        <div className={styles.container}>
            <Navbar/>
            <WaFloat/>
            <div className="banner-screen">
                <Image src="/bg-news-detail.png" width="100%" height="100%" layout="fill" objectFit="cover"/>
                <div style={{ position: 'absolute', width: 'inherit', height: 'inherit', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>{detail?.title}</h4>
                </div>
            </div>

            <div className="container-fluid flex-wrap py-5 benefit-container" style={{ backgroundColor: '#F2F3F7' }}>
                <div className={"col-12 general-container "  + (+window.innerWidth < 550 ? "pb-2" : "pb-3")}>
                    <h4 className="porto-detail-hist"  style={{ color: '#F48C06' }}>Berita &gt; {detail?.title}</h4>
                    <h1 className="porto-detail-title" style={{ fontWeight: 600 }}>Project Kami</h1>
                </div>

                <div className="row">
                    <div className="col-xl-8 col-md-8 col-sm-12 general-container pb-3 d-flex flex-column">
                        <img src={detail?.banner_source} width="100%" layout="fill" />
                        <h1 style={{ fontWeight: 600, marginTop: '1rem' }} className={"mb-0 pt-1 lh-1 porto-detail-title "}>{detail?.title}</h1>
                        <span className={"porto-detail-date "+ (+window.innerWidth < 550 ? "mb-2" : "mb-3")}><b>Diposting pada</b> {moment(detail?.created_at).format('LL')}</span>
                        <div style={{ whiteSpace: 'pre-line' }} dangerouslySetInnerHTML={{ __html: detail?.content }} ></div>
                        
                        <div className="row">
                            <div className="col-12 pt-3 pb-3">
                                <div className="porto-detail-title fw-bold">
                                    Tags&nbsp;
                                    {
                                        detail?.tags?.map((tag, idx) => (
                                            <button className='btn btn-sm' style={{backgroundColor: 'rgb(176, 16, 41)', color: 'white', marginLeft: 3}}>{tag.trim()}</button>
                                        ))
                                    }
                                </div>
                            </div>
                            {/* <div className="col-xl-11 col-ld-11 col-md-11 col-sm-10 col-10">
                                <div className="row">
                                    {
                                        detail?.tags?.map((tag, idx) => (
                                            <div className="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-3" key={idx} style={{paddingLeft: 0}}>
                                                <button className="filled-btn btn" style={{width: '100%'}}>{tag.trim()}</button>
                                            </div>
                                        ))
                                    }
                                    
                                </div>
                                
                            </div> */}
                        </div>
                    </div>
                    <div className="col-xl-4 col-md-4 col-sm-12 col-12 px-3 pb-3 ">
                        <h1 style={{ fontWeight: 600, color: '#B01029' , textAlign: 'center'}}>Berita <span style={{textDecoration: 'underline'}}>Terkini</span></h1>
                        
                        {
                            latest.map((data,idx) => (
                                <Link href={`/news/details?slug=${data.slug}`} key={idx}>
                                    <div className="row mb-3" >
                                        <div className="col-3 px-0 pe-3">
                                            <img src={data.thumbnail_source} style={{width: '80px', height: '80px'}}/>
                                        </div>
                                        <div className={"col-9 px-0 " + (+window.innerWidth < 550 ? "ps-4" : "")}>
                                            <div className="fs-5 elipsis-2 fw-bold">{data.title}</div>
                                            <p className="mb-0">{moment(data.created_at).format('LL')}</p>
                                        </div>
                                    </div>
                                </Link>
                            ))
                        }
                        
                    </div>
                </div>
                
            </div>
            
            <Footer/>
            
        </div>
    )
}