import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'

import Api from '../utils/Api'
import styles from '../styles/Home.module.css'
import Navbar from '../component/navbar'
import Footer from '../component/footer'
import WaFloat from '../component/wa-float'

export default function Home() {
    
    const [data, setdata] = useState([])
    const [responsive, setresp] = useState("")

    useEffect(() => {
        getdata()
        setresp(window.innerWidth < 500 ? "mobile" : (window.innerWidth <= 950 && "tablet"))
      }, [])
    
      const getdata = async () => {
        try {
          let fetch = await Api.get('/about-us')
            
         
          if(fetch) {
            setdata(fetch.data.response)
          }
        } catch (error) {
          console.log(error)
        }
      }
    return (
        <div className={styles.container}>
            <Navbar/>
            <WaFloat/>
            <div className="banner-screen">
                <Image src="/bg-about.png" width="100%" height="100%" layout="fill" objectFit="cover"/>
                <div style={{ position: 'absolute', width: 'inherit', height: 'inherit', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                    <h1 style={{ fontWeight: 700, color: 'white', marginBottom: 50 }}>PERUSAHAAN KAMI</h1>
                    {/* <h3 style={{ color: 'white', fontSize: responsive == "mobile" ? "1.2rem" : "calc(1.3rem + 0.6vw)"  }}>Perkenalkan Kami Raja Rental Indonesia</h3>
                    <h3 style={{ color: 'white', fontSize: responsive == "mobile" ? "1.2rem" : "calc(1.3rem + 0.6vw)"  }}>Tempat Sewa Elektronik Terpercaya</h3> */}
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Perkenalkan Raja Rental Indonesia</h4>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Tempat Sewa Elektronik Terpercaya</h4>
                </div>
            </div>

            <div className="container-fluid  flex-wrap py-5 benefit-container" style={{ backgroundColor: '#F2F3F7' }}>
                <div className="col-12 general-container pb-3">
                    <h4 style={{ color: '#F48C06' }}>Tentang Kami &gt; Perusahaan</h4>
                    <h1 style={{ fontWeight: 700 }}>Sejarah Singkat <span style={{ color: '#B01029' }}>Raja Rental Indonesia</span></h1>

                    <p style={{ marginTop: '2vw', whiteSpace: 'pre-line'  }}>{data?.description}</p>
                </div>
            </div>

            <Footer/>
        </div>
    )
}