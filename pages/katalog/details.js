import React, { useState, useEffect } from 'react';
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import Slider from "react-slick";

import Api from '../../utils/Api'
import styles from '../../styles/Home.module.css'
import Navbar from '../../component/navbar'
import Footer from '../../component/footer'
import WaFloat from '../../component/wa-float'

export default function Home() {
    let arr = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    const [howto, sethowto] = useState([])
    const [Wa, setWa] = useState({})
    const [products, setproducts] = useState({})
    const [variant, setvar] = useState({})
    const settings = {
        customPaging: function(i) {
          return (
            <a>
              <img src={products?.images[i]} style={{width: '100%'}} />
            </a>
          );
        },
        dots: true,
        dotsClass: "slick-dots slick-thumb",
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
      };

    const router = useRouter()
    const {slug} = router.query

    function SamplePrevArrow(props) {
        const { className, style, onClick } = props;
        return (
            <div
                className={className}
                style={{ ...style, display: "block", left: '-50px', color: 'transparent' }}
                onClick={onClick}>
                <img src="/vector-left.png" style={{ width: 30, height: 45 }} />
            </div>
        );
    }

    function SampleNextArrow(props) {
        const { className, style, onClick } = props;
        return (
            <div
                className={className}
                style={{ ...style, display: "block", right: '-35px', color: 'transparent' }}
                onClick={onClick}>
                <img src="/vector-right.png" style={{ width: 30, height: 45 }} />
            </div>
        );
    }

    // const settings = {
    //     dots: false,
    //     infinite: true,
    //     speed: 500,
    //     slidesToShow: 1,
    //     slidesToScroll: 3,
    //     prevArrow: <SamplePrevArrow />,
    //     nextArrow: <SampleNextArrow />
    // };


    useEffect(() => {
        gethowto()
        getProduct()
        
    }, [])

    const gethowto = async () => {
        try {
            let fetch = await Api.get('/how-to-order')

            if (fetch) {
                sethowto(fetch.data.response.sort((a, b) => (a.step - b.step)))
            }
        } catch (error) {

        }
    }

    const getProduct = async () => {
        try {
            let param = router.asPath?.split("?")?.[1]?.split("=")
            let prodId = param[1].split('-')
            let fetch = await Api.get('/product/' + prodId[prodId.length-1])
            let getWa = await Api.get('/whatsapp-contact')
            if (fetch) {
                let data = fetch.data.response
                let tempImage = []
                let tempVar = []
                let chooseVar = {}
                var varText = ''
                for(let key in data) {
                    if(key.includes("image") && !key.includes("main")) {
                        if(data[key] === null || data[key] === "null") {}
                        else {
                           tempImage.push(data[key]) 
                        }
                        
                    }
                }
                let variant = []
                for(let key in data.variants) {
                    varText = varText + key + " " + data.variants[key][0] + " ";
                    chooseVar[key] = data.variants[key][0]
                    tempVar.push({
                        name: key,
                        value: data.variants[key]
                    })
                }
                var text = `Halo, apakah ${data.name} dengan spesifikasi ${varText} masih tersedia?`;
                setWa({...getWa.data.response, pretext: text});
                // getWa()
                setproducts({...data, variants: tempVar, images: tempImage})
                setvar(chooseVar)
            }
        } catch (error) {

        }
    }

    const choosingVar = (key, val) => {
        let temp = {...variant, [key]: val}
        let text = ''
        for(let key in temp) {
                text = text + key + ' ' + temp[key] + ' '
        }
        setWa({...Wa, pretext: `Halo saya berminat dengan ${products.name} ${text}. Bisakah saya meminta detail dari item tersebut? Terima kasih.`})
        setvar(temp)
    }

    return (
        <div className={styles.container}>
            <Navbar />
            <WaFloat/>
            <div className="banner-screen">
                <Image src="/bg-detail-katalog.png" width="100%" height="100%" layout="fill" objectFit="cover"/>
                <div style={{ position: 'absolute', width: 'inherit', height: 'inherit', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                    <h1 style={{ fontWeight: 700, color: 'white', marginBottom: 50 }}>DETIL PRODUK</h1>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Silahkan Lihat Detil</h4>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Produk Terbaik Dari Kami</h4>
                </div>
            </div>

            <div className="container-fluid  flex-wrap py-5 benefit-container" style={{ backgroundColor: '#F2F3F7' }}>
                <div className="col-12 general-container pb-3">
                    <h4 style={{ color: '#F48C06' }}>Katalog &gt; Detil Produk</h4>
                    <h1 style={{ fontWeight: 700 }}>{products.name}</h1>
                </div>

                <div className="col-12 general-container pb-3 ">
                    <div className="row">
                        <div className="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 benefit-container pb-3 kat-prod-slider">
                            {/* <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                                <div className="carousel-inner">
                                    {
                                        products?.images?.map((image, idx) => (
                                            <div className={"carousel-item" + (idx == 0 ? " active" : "")} key={idx}>
                                                <img src={image} className="d-block w-100" alt="..." />
                                            </div>
                                        ))
                                    }
                                </div>
                                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Previous</span>
                                </button>
                                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Next</span>
                                </button>
                            </div> */}
                            <div style={{width: '100%'}}>
                                <Slider {...settings}>
                                    {
                                        products?.images?.map((image, idx) => (
                                            <div className="" key={idx}>
                                            {/* <div className={"carousel-item" + (idx == 0 ? " active" : "")} key={idx}> */}
                                                <img src={image} className="d-block w-100" alt="..." />
                                            {/* </div> */}
                                            </div>
                                        ))
                                    }
                                </Slider>
                            </div>
                            
                        </div>

                        <div className={"col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12  benefit-container pb-3 " + (window.innerWidth < 500 ? "pt-5" : "")}>
                            <h2 style={{ fontWeight: 700 }}>Spesifikasi</h2>
                            {
                                products?.variants?.map((data, idx) => (
                                    <React.Fragment key={idx}>
                                        <h5 className="mb-0" style={{ fontWeight: 400 }}>{data.name}</h5>
                                        <div className="row mb-3 ps-2">
                                            {
                                                data?.value?.map((val,id) => (
                                                    <div
                                                        onClick={() => choosingVar(data.name, val)}
                                                        key={id}
                                                        className="me-3"
                                                        style={
                                                            variant[data.name] == val ? 
                                                            {
                                                            backgroundColor: "#B01029",
                                                            borderRadius: 25,
                                                            color: 'white',
                                                            width: 'auto',
                                                            padding: "5px 11px 3px 11px",
                                                            height: 'fit-content',
                                                            marginTop: 5
                                                        } : {
                                                            color: "#B01029",
                                                            borderRadius: 25,
                                                            border: '1px solid #B01029',
                                                            width: 'auto',
                                                            padding: "4px 11px 2px 11px",
                                                            height: 'fit-content',
                                                            marginTop: 5
                                                        }}
                                                    >
                                                        <span>{val}</span>
                                                        
                                                    </div>
                                                ))
                                            }
                                        </div>
                                    </React.Fragment>
                                ))
                            }
                            {/* <h3 style={{ fontWeight: 400 }}>Varian CPU</h3>
                            <div className="row">
                                <div
                                    className="px-4 py-1 me-3"
                                    style={{
                                        backgroundColor: "#B01029",
                                        borderRadius: 25,
                                        color: 'white',
                                        width: 'auto',
                                    }}
                                >
                                    AMD Rayzen
                                </div>
                                <div
                                    className="px-4 py-1 me-3"
                                    style={{
                                        color: "#B01029",
                                        borderRadius: 25,
                                        border: '1px solid #B01029',
                                        width: 'auto',
                                    }}
                                >
                                    AMD Rayzen
                                </div>
                            </div> */}

                            <h2 className="mt-4" style={{ fontWeight: 700  }}>Deskripsi</h2>
                            <h5 style={{ fontWeight: 400, whiteSpace: 'pre-line' }}>{products?.long_desc}</h5>

                            <button type="button"
                                onClick={() => window.open(`https://wa.me/${Wa.wa_number}?text=${encodeURI(Wa.pretext)}`, "", "_blank")}
                                className="btn d-flex align-items-center justify-content-center px-5 py-2 mt-4" style={{
                                    backgroundColor: "#0CBC6A",
                                    color: "#fff",
                                    border: '0',
                                    fontWeight: 'bold',
                                    fontSize: '1.1rem'
                                }}>
                                <Image src="/wa-white.png" width={'25vw'} height={'25vw'} />
                                <span style={{ marginLeft: '0.7rem', fontWeight: 600 }}>Buat Penawaran</span></button>
                        </div>

                    </div>
                </div>
            </div>

            <div className="container-fluid  flex-wrap py-5">
                <div className="col-12 text-center pb-2">
                    <h2 style={{ fontWeight: 400 }}>Mau dapet harga <b style={{ color: '#B01029' }}>ekslusif?</b></h2>
                    <h1 style={{ fontWeight: 400 }}>Tunggu apalagi...langsung aja</h1>
                </div>
                <div className="col-12 text-center">
                    <button 
                    onClick={() => window.open(`https://wa.me/${Wa.wa_number}?text=${encodeURI(Wa.pretext)}`, "", "_blank")}
                    type="button" 
                    className="btn px-5 py-1" style={{
                        backgroundColor: "#B01029",
                        color: "white",
                        fontSize: '1.1rem',
                        alignSelf: 'center'
                    }}>Klik Disini</button>
            </div>
            </div>

            <div className="container-fluid  flex-wrap py-5 general-container" style={{ backgroundColor: '#F2F3F7' }}>
                <div className="col-12 benefit-container pb-3 pt-5">
                    <h1 className="fw-bold mb-4">Bagaimana Cara Ordernya?</h1>
                    <div className="row justify-content-center">
                        {
                            howto.map((data,idx) => (
                                <React.Fragment key={idx}>
                                    <div className="col-3 d-flex flex-column align-items-center mb-5">
                                        <div className="how-to-ava d-flex fw-bold align-items-center justify-content-center rounded-circle">{data.step}</div>
                                        <p className="how-to-text text-center mt-4">{data.description}</p>
                                    </div>
                                    {
                                        data.step % 3 !== 0 ? (
                                            <div className="col-1 pt-3" style={window.innerWidth < 500 ? {padding: 0} : {}}>
                                                {
                                                    window.innerWidth < 500 ? (
                                                        <Image src="/red-arrow-right.png" width={100} height={70} />
                                                    ) : (
                                                        <Image src="/red-arrow-right.png" width={"60vw"} height={"50vw"} />
                                                    )
                                                }
                                                
                                                
                                            </div> 
                                        ) : null
                                    }
                                </React.Fragment>
                            ))
                        }
                        
                    </div>
                </div>
            </div>


            <Footer />
        </div>
    )
}