import React, { useState, useEffect } from 'react';
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'

import Api from '../../utils/Api'
import styles from '../../styles/Home.module.css'
import Navbar from '../../component/navbar'
import Footer from '../../component/footer'
import WaFloat from '../../component/wa-float'
import { fill } from 'lodash';

export default function Home() {
    let arr = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    const [howto, sethowto] = useState([])
    const [products, setproducts] = useState([])
    const [category, setcategory] = useState([])
    const [total_page, settotal] = useState(1)
    const [query, setquery] = useState({            
        pagination: true,
        page: 1,
        row: 8
    })
    const [activeCat, setactivecat] = useState({})
    const [search, setsearch] = useState('')
    const [responsive, setresp] = useState("")

    const router = useRouter()

    useEffect(() => {
      gethowto()
      getProduct(router.asPath)
      getCategory()
      setresp(window.innerWidth < 500 ? "mobile" : (window.innerWidth <= 700 ? "tablet" : ""))
      const handleRouteChange = (url, { shallow }) => {
        getProduct(url)
      }
  
      router.events.on('routeChangeStart', handleRouteChange)
  
    }, [])
  
    const gethowto = async () => {
      try {
        let fetch = await Api.get('/how-to-order')
  
        if(fetch) {
          sethowto(fetch.data.response.sort((a,b) => (a.step - b.step)))
        }
      } catch (error) {
        
      }
    }

    const getProduct = async (url) => {
        let params = {...query}
        let queryTemp = {...query}
        let allParam = url?.split("?")?.[1]?.split("&")
        
        if(allParam) {
            for(let i = 0; i < allParam.length; i++) {
                let splitQuery = allParam[i].split("=")
                let key = splitQuery[0] == 'cat' ? 'category_id' : (splitQuery[0] == 'kw' ? 'search' : splitQuery[0])
                params[key] = splitQuery[1]
                queryTemp[splitQuery[0]] = splitQuery[1]
                if(splitQuery[0] == "kw") {setsearch(splitQuery[1])}
            }
        }
        
        setquery(queryTemp)

        try {
          let fetch = await Api.get('/product', {
            params
          })
    
          if(fetch) {
            settotal(Math.ceil(fetch.data.response.count/8))
            setproducts(fetch.data.response.rows)
          }
        } catch (error) {
          
        }
      }

    const getCategory = async (url) => {
    try {
        let fetch = await Api.get('/product-category?pagination=false')

        if(fetch) {
            setcategory(fetch.data.response)
            let allParam = router?.asPath?.split("?")?.[1]?.split("&")
            
            if(allParam) {
                for(let i = 0; i < allParam.length; i++) {
                    let splitQuery = allParam[i].split("=")
                    if(splitQuery[0] == 'cat') {
                        setactivecat(fetch.data.response.find(a => a.id == splitQuery[1]))
                    }
                }
            }
        }
    } catch (error) {
        console.log(error)
    }
    }

    const replaceQuery = (name, value) => {
        let params = {...query}
        params[name] = value
        if(name !== 'page') {
            params.page = 1
        }
        if(name == 'cat') {
            setactivecat(category?.find(a => a.id == value))
        }
        let queryParams = ''
        for(let key in params) {
            queryParams = queryParams + `${queryParams.length > 0 ? '&' : ''}${key}=${params[key]}`
        }

        router.replace('/katalog?' + queryParams, undefined, {scroll: false})

    }
    

    return (
        <div className={styles.container}>
            <Navbar/>
            <WaFloat/>
            <div className="banner-screen">
                <Image src="/bg-produk.png" width="100%" height="100%" layout="fill" objectFit="cover"/>
                <div style={{ position: 'absolute', width: 'inherit', height: 'inherit', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                    <h1 style={{ fontWeight: 700, color: 'white', marginBottom: 50 }}>KATALOG</h1>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Mari Lihat <b>LINEUP</b> Produk Terkini</h4>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Dari Raja Rental Indonesia</h4>
                </div>
            </div>

            <div className="container-fluid  flex-wrap py-5 general-container" style={{ backgroundColor: '#F2F3F7' }}>
                <div className="col-12 benefit-container pb-3">
                    <h4 style={{ color: '#F48C06' }}>Katalog</h4>
                    <h1 style={{ fontWeight: 700 }}>Peralatan Terbaik Hanya Untuk Anda</h1>
                </div>

                <div className="col-12 benefit-container pb-3">
                    <div className="row">
                        {
                            category.map((data, idx) => (
                                <div style={responsive == "tablet" || responsive == "mobile" ? {paddingLeft: 0} : {}} className={"col-xl-2 col-md-3 col-sm-2 col-4 text-center " + (responsive == "tablet" || responsive == "mobile" ? "mb-3" : "mb-4")}key={idx}>
                                    <button type="button" className="btn" 
                                    onClick={() => replaceQuery('cat', data.id)} className={query.cat == data.id ? "prod-cat-card-active" : "prod-cat-card"}>{data.name}</button>
                                </div>
                            ))
                        }

                    </div>
                </div>

                <div className="col-12 benefit-container pb-3 ">
                    <div className="row flex-row-reverse justify-content-between">
                        
                        <div className="col-xl-3 col-md-3 col-7">
                            <div className="input-group" >
                                <button className="btn" style={{ border: '1px solid #B01029', borderRightWidth: 0 }} type="button" id="button-addon1"><Image src="/magnify_glass.png" width={15} height={15} style={{verticalAlign: 'middle'}}/></button>
                                <input 
                                value={search}
                                onChange={(e) => setsearch(e.target.value)}
                                onKeyDown={event => {if(event.key === 'Enter'){replaceQuery('kw', search)}}}
                                type="text" 
                                className="form-control kat-search" 
                                placeholder={"Cari barang di " + (activeCat?.name || 'Semua')}
                                aria-describedby="button-addon1" 
                                style={{ 
                                    border: '1px solid #B01029', 
                                    backgroundColor: 'transparent', 
                                    borderLeftWidth: '0' 
                                }}
                                />
                            </div>
                        </div>
                        <div className="col-xl-3 col-md-3 col-5">
                            {query.kw && 
                                <p className="kat-prod-title">Hasil Pencarian untuk <span className="color-primary">{query.kw}</span></p>
                            }
                            
                        </div>
                    </div>
                </div>

                <div className="col-12 benefit-container pb-3 ">
                    <div className="row">
                        {
                            products?.length == 0 &&
                            <div className="col-12 text-center">
                                <h1 className=" mb-4">Oops, barang belum tersedia</h1>
                            </div>
                        }
                        {
                            products.map((data, idx) => (
                                <div className="col-xl-2 col-md-4 col-sm-5 col-6 mb-4" key={idx} style={window.innerWidth < 500 ? {paddingLeft: 0} : {paddingLeft: '', paddingRight: ''}}>
                                    <div className="d-flex flex-column align-items-center text-center shadow-sm" style={{
                                        backgroundColor: 'white',
                                        borderRadius: 10
                                    }}>
                                        <div className="kat-prod-img" >
                                            <img src={data.main_image_source} width={"100%"} height={"100%"} layout="fill" style={{borderTopLeftRadius: 10, borderTopRightRadius: 10, objectFit: "contain"}}/>
                                        </div>
                                        <div style={{height: window.innerWidth < 550 ? "50px" : "60px"}}>
                                            <div className={"fw-bold elipsis-2 kat-prod-title "  + (responsive == "tablet" || responsive == "mobile" ? "my-2" : "my-2")} style={{marginLeft: '5%', marginRight: '5%'}}>{data.name}</div>
                                        </div>
                                        
                                        <button 
                                        onClick={() => {window.location.href = `/katalog/details?slug=${data.name.replace(/ /g, "-") + '-' + data.id}`}}
                                        type="button" 
                                        className={"btn filled-btn katalog-btn " + (responsive == "tablet" || "mobile" ? "mb-2" : "mb-4")}
                                        style={{width: '90%'}}>Lihat Selengkapnya</button>
                                    </div>

                                </div>
                            ))
                        }

                    </div>
                </div>

                {
                    total_page > 1 && (
                        <div className="col-12 px-5 pb-3 ">
                            <div className="row justify-content-center">
                                <div className="col-3 d-flex justify-content-center">
                                    <nav aria-label="Page navigation example">
                                        <ul className="pagination">
                                            <li className={"page-item " + (query.page == 1 ? "disabled" : "")}>
                                                <a className="page-link" aria-label="Previous" onClick={() => replaceQuery('page', +query.page - 1)}>
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                            {
                                                query.page == 1 ?  null :(
                                                    <React.Fragment>
                                                        {
                                                            query.page == total_page &&
                                                            Number(query.page) - 1 !== 0 &&
                                                            total_page > 2 &&
                                                            <li className="page-item" onClick={() => replaceQuery('page', +query.page - 2)}><a className="page-link" href="">{Number(query.page)-2}</a></li>
                                                        }
                                                        
                                                        <li className="page-item" onClick={() => replaceQuery('page', +query.page - 1)}><a className="page-link" href="">{Number(query.page)-1}</a></li>
                                                    </React.Fragment>
                                                )
                                            }
                                            <li className="page-item active"><a className="page-link" href="#">{query.page}</a></li>
                                            {
                                                +query.page == total_page ?  null :(
                                                    <React.Fragment>
                                                        <li className="page-item" onClick={() => replaceQuery('page', +query.page + 1)}><a className="page-link" href="">{Number(query.page)+1}</a></li>
                                                        {
                                                            query.page == 1 &&
                                                            Number(query.page) + 1 !== total_page &&
                                                            <li className="page-item" onClick={() => replaceQuery('page', +query.page + 2)}><a className="page-link" href="">{Number(query.page)+2}</a></li>
                                                        }
                                                    </React.Fragment>
                                                )
                                            }
                                            <li className={"page-item " + (Number(query.page) === total_page ? "disabled" : "")}>
                                                <a className="page-link" aria-label="Next" onClick={() => replaceQuery('page', +query.page + 1)}>
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    )
                }
                

                <div className="col-12 benefit-container pb-3 pt-5">
                    <h1 className="fw-bold mb-4">Bagaimana Cara Ordernya?</h1>
                    <div className="row justify-content-center">
                        {
                            howto.map((data,idx) => (
                                <React.Fragment key={idx}>
                                    <div className="col-3 d-flex flex-column align-items-center mb-5">
                                        <div className="how-to-ava d-flex fw-bold align-items-center justify-content-center rounded-circle">{data.step}</div>
                                        <p className="how-to-text text-center mt-4">{data.description}</p>
                                    </div>
                                    {
                                        data.step % 3 !== 0 ? (
                                            <div className="col-1 pt-3" style={responsive == "mobile" ? {padding: 0} : {}}>
                                                {
                                                    responsive == "mobile" ? (
                                                        <Image src="/red-arrow-right.png" width={100} height={70} />
                                                    ) : (
                                                        <Image src="/red-arrow-right.png" width={"60vw"} height={"50vw"} />
                                                    )
                                                }
                                                
                                                
                                            </div> 
                                        ) : null
                                    }
                                </React.Fragment>
                            ))
                        }
                        
                    </div>
                </div>

            </div>

            <Footer/>
        </div>
    )
}