import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import Slider from "react-slick";
import { useRouter } from 'next/router'
import { ScrollMenu, VisibilityContext } from 'react-horizontal-scrolling-menu';

import styles from '../styles/Home.module.css'
import Api from '../utils/Api'
import Navbar from '../component/navbar'
import Footer from '../component/footer'
import WaFloat from '../component/wa-float'
import { LeftArrow, RightArrow } from "../component/arrow";

export default function Home() {
  let arr = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  const [banners, setBanners] = useState([])
  const [benefit, setbenefit] = useState([])
  const [services, setservices] = useState([])
  const [lineup, setlineup] = useState([])
  const [testi, settesti] = useState([])
  const [partner, setpartner] = useState([])
  const [porto, setporto] = useState([])
  const [professional, setprofessional] = useState([])
  const [responsive, setresp] = useState("")

  const router = useRouter()

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", left: '-50px', color: 'transparent' }}
        onClick={onClick}>
        <img src="/vector-left.png" style={{ width: 30, height: 45 }} />
      </div>
    );
  }

  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", right: '-35px', color: 'transparent' }}
        onClick={onClick}>
        <img src="/vector-right.png" style={{ width: 30, height: 45 }} />
      </div>
    );
  }

  function onWheel(apiObj, ev) {
    const isThouchpad = Math.abs(ev.deltaX) !== 0 || Math.abs(ev.deltaY) < 15;
    if (isThouchpad) {
        ev.stopPropagation();
        return;
    }
    if (ev.deltaY < 0) {
        apiObj.scrollNext();
    }
    else if (ev.deltaY > 0) {
        apiObj.scrollPrev();
    }
  }

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    // prevArrow: <SamplePrevArrow/>,
    // nextArrow: <SampleNextArrow/>
  };

  useEffect(() => {
    getBanner()
    getBenefit()
    getHomePro()
    getServices()
    getLineup()
    getTesti()
    getPartner()
    getPorto()
    setresp(window.innerWidth < 500 ? "mobile" : (window.innerWidth < 800 && "tablet"))
  }, [])

  const getBanner = async () => {
    try {
      let fetch = await Api.get('/home-banner')

      if (fetch) {
        setBanners(fetch.data.response)
      }
    } catch (error) {

    }
  }

  const getBenefit = async () => {
    try {
      let fetch = await Api.get('/benefit')

      if (fetch) {
        setbenefit(fetch.data.response.rows)
      }
    } catch (error) {

    }
  }

  const getHomePro = async () => {
    try {
      let fetch = await Api.get('/home-professional')

      if (fetch) {
        setprofessional(fetch.data.response)
      }
    } catch (error) {

    }
  }

  const getServices = async () => {
    try {
      let fetch = await Api.get('/service', {
        params: {
          page: 1,
          order_type: 'DESC'
        }
      })

      if (fetch) {
        setservices(fetch.data.response.rows)
      }
    } catch (error) {

    }
  }

  const getLineup = async () => {
    try {
      let fetchLaptop = await Api.get('/product', {
        params: {
          pagination: false,
          category_id: 1,
          is_lineup: true,
          page: 1,
          row: 6,
          order_type: 'DESC'
        }
      })
      let fetchPC = await Api.get('/product', {
        params: {
          pagination: false,
          category_id: 2,
          is_lineup: true,
          page: 1,
          row: 6,
          order_type: 'DESC'
        }
      })

      if (fetchLaptop) {
        let data = []
        data.push({
          category: 'Laptop',
          rows: [...fetchLaptop.data.response.rows, ...fetchLaptop.data.response.rows]
        })
        if (fetchPC) {
          data.push({
            category: 'PC',
            rows: fetchPC.data.response.rows
          })
        }

        setlineup(data)
      }
    } catch (error) {

    }
  }

  const getTesti = async () => {
    try {
      let fetch = await Api.get('/testimonial')

      if (fetch) {
        // console.log(fetch.data, 'testi')
        settesti(fetch.data.response.rows)
      }
    } catch (error) {

    }
  }

  const getPartner = async () => {
    try {
      let fetch = await Api.get('/partner?pagination=false')

      if (fetch) {
        setpartner(fetch.data.response.rows)
      }
    } catch (error) {

    }
  }

  const getPorto = async () => {
    try {
      let fetch = await Api.get('/article/portfolio')

      if (fetch) {
        // console.log(fetch.data, 'porto')
        setporto(fetch.data.response.rows)
      }
    } catch (error) {

    }
  }

  return (
    <div className={styles.container}>
      <Navbar />
      <WaFloat />
      <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">

        <div className="carousel-inner">
          {
            banners.map((banner, idx) => (
              <div className={"carousel-item" + (idx == 0 ? " active" : "")} key={idx} data-bs-interval="5000">
                <a href={banner.url_to}>
                  <img src={banner.image_source} className="d-block w-100" alt={banner.name} />
                </a>
              </div>
            ))
          }
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>

      <div className="container-fluid  flex-wrap py-5 general-container" style={{ backgroundColor: '#F2F3F7' }}>
        <div className="col-12 text-center pb-5">
          <h1 className="fw-bold">Alasan Memilih</h1>
          <h1 className="fw-bold" style={{ color: '#B01029' }}>Raja Rental Indonesia</h1>
        </div>

        <div className="row justify-content-center benefit-container">
          {
            benefit.map((data, idx) => (
              <div className="col-xl-4 col-md-6 mb-5 px-5 text-center" key={idx}>
                <img src={data.icon_source} style={{ width: 50, height: 50 }} />
                <h3 style={{ color: data.title_color }}><b>{data.title}</b></h3>
                <span>{data.subtitle}</span>
              </div>
            ))
          }
        </div>

      </div>

      <div className="container-fluid  flex-wrap py-5">
        <div className="col-12 text-center pb-4">
          <h1 className="fw-bold">Profesionalitas Adalah Segalanya</h1>
        </div>

        <div className="row justify-content-center">
          {
            professional.map((data, idx) => {
              if (idx !== 1) {
                return (
                  <div className={"col-xl-3 col-md-3 col-sm-6 text-center " + (responsive === "mobile" ? "mb-3" : "")}>
                    <h3 className="color-primary fw-bold">{data.title}</h3>
                    <span>{data.subtitle}</span>
                    {
                      responsive === "mobile" &&
                      <hr style={{ width: "20%", margin: '1rem auto' }} />
                    }
                  </div>
                )
              } else {
                return (
                  <div className={"col-xl-3 col-md-3 col-sm-6 text-center " + (responsive !== "mobile" ? "border border-dark border-2 border-top-0 border-bottom-0" : "mb-3")}>
                    <h3 className="color-primary fw-bold">{data.title}</h3>
                    <span>{data.subtitle}</span>
                    {
                      responsive === "mobile" &&
                      <hr style={{ width: "20%", margin: '1rem auto' }} />
                    }
                  </div>
                )
              }
            })
          }

          {/* <div className="col-3 text-center">
            <h3 style={{ color: '#B01029' }}><b>2500+</b></h3>
            <span>Event Terlaksana</span>
          </div> */}
        </div>
      </div>

      <div className="container-fluid  flex-wrap py-5 general-container" style={{ backgroundColor: '#F2F3F7' }}>
        <div className="row justify-content-between align-items-center pb-5 benefit-container">
          <div className="col-xl-7 col-md-7 col-sm-12 pb-3 service-title-card">
            <h4 className="color-secondary">Servis Yang Tersedia</h4>
            <h1 className="fw-bold service-title">Kami Menyediakan <span className="color-primary">Solusi {window.innerWidth < 576 && 'terlengkap'}</span> {window.innerWidth < 576 && 'Untuk Anda'}</h1>
            {window.innerWidth > 576 && <h1 className="fw-bold"><span className="color-primary">terlengkap</span> Untuk Anda</h1>}
          </div>

          <div className={"col-xl-5 col-md-5 col-sm-12 d-flex justify-content-end " + (responsive !== "mobile" ? "pe-5" : "")}>
            <Link href="/katalog">
              <button type="text" className="btn filled-btn more-btn">Lihat Semua</button>
            </Link>
          </div>
        </div>

        <div className="row justify-content-center benefit-container">
          {
            services?.map((data, idx) => (
              <div className={"col-xl-4 col-md-6 mb-5 benefit-container " + (responsive === "mobile" ? "pb-3" : "pb-5")} key={idx}>
                <h4><img src={data.icon_source} /><b style={{ marginLeft: 12 }}>{data.name}</b></h4>
                <span>{data.short_desc}</span>
              </div>
            ))
          }
        </div>

      </div>

      <div className="container-fluid  flex-wrap py-5 general-container" style={{ backgroundImage: "url('/bg_lineup.png')", backgroundRepeat: 'no-repeat', backgroundSize: '100% 100%', backgroundAttachment: 'cover' }}>
        <div className="col-12 text-center pb-5">
          <h1 className="fw-bold">Yuk Lihat <span className="color-primary">LINEUP</span> Produk Kami</h1>
          <h4 className="color-secondary">Kami Pastikan Produk Kami Yang Terbaik Di kelasnya</h4>
        </div>

        <div className="col-12 text-center pb-1">
          <div className="row justify-content-between align-items-center benefit-container">
            <div className="col-xl-4 col-md-4 col-sm-12 mb-5 ps-5 pe-3 pb-2">
              <Link href="/katalog?cat=1">
                <img src="/laptop_cat.png" 
                style={{width: '90%', height: '90%'}}
                // style={{ width: responsive == "mobile" ? "80%" : '90%', height: responsive == "mobile" ? "80%" : '90%' }} 
                />
              </Link>

            </div>

            <div className="col-xl-8 col-md-8 col-sm-12 mb-5 benefit-container pb-2">
              <ScrollMenu Footer={<div></div>} 
              LeftArrow={LeftArrow}
              RightArrow={RightArrow}
              onWheel={onWheel}>
                {
                  lineup?.[0]?.rows.map((a, idx) => (

                    <div className="lineup-card mx-2" key={idx} style={window.innerWidth < 500 ? { paddingLeft: 0 } : { paddingLeft: '', paddingRight: '' }}>
                      <div className="d-flex flex-column align-items-center text-center shadow-sm" style={{
                        backgroundColor: 'white',
                        borderRadius: 10
                      }}>
                        <div className="lineup-img" >
                          <img src={a.main_image_source} width={"100%"} height={"100%"} layout="fill" style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }} />
                        </div>
                        <div style={{ height: window.innerWidth < 550 ? "50px" : "60px" }}>
                          <div className={"fw-bold elipsis-2 kat-prod-title " + (responsive == "tablet" || responsive == "mobile" ? "my-2" : "my-2")} style={{ marginLeft: '5%', marginRight: '5%' }}>{a.name}</div>
                        </div>

                        <button 
                            onClick={() => router.push('/katalog/details?slug=' + a.name.replace(/ /g, "-") + '-' + a.id)}
                            type="button" 
                            className="btn mb-2 py-1 outline-btn lineup-btn">Lihat Detail</button>
                      </div>

                    </div>

                    
                  ))
                }                
              </ScrollMenu>
            </div>


          </div>

          <div className="row justify-content-between align-items-center benefit-container">
            <div className="col-xl-4 col-md-4 col-sm-12 mb-5 ps-5 pe-3 pb-2">
              <Link href="/katalog?cat=2">
                <img src="/pc_cat.png" 
                style={{width: '90%', height: '90%'}}
                // style={{ width: responsive == "mobile" ? "80%" : '90%', height: responsive == "mobile" ? "80%" : '90%' }} 
                />
              </Link>
            </div>

            <div className="col-xl-8 col-md-8 col-sm-12 mb-5 benefit-container pb-2">
            <ScrollMenu Footer={<div></div>}
              LeftArrow={LeftArrow}
              RightArrow={RightArrow}
              onWheel={onWheel}>
                {
                  lineup?.[1]?.rows.map((a, idx) => (

                    <div className="lineup-card mx-2" key={idx} style={window.innerWidth < 500 ? { paddingLeft: 0 } : { paddingLeft: '', paddingRight: '' }}>
                      <div className="d-flex flex-column align-items-center text-center shadow-sm" style={{
                        backgroundColor: 'white',
                        borderRadius: 10
                      }}>
                        <div className="lineup-img" >
                          <img src={a.main_image_source} width={"100%"} height={"100%"} layout="fill" style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }} />
                        </div>
                        <div style={{ height: window.innerWidth < 550 ? "50px" : "60px" }}>
                          <div className={"fw-bold elipsis-2 kat-prod-title " + (responsive == "tablet" || responsive == "mobile" ? "my-2" : "my-2")} style={{ marginLeft: '5%', marginRight: '5%' }}>{a.name}</div>
                        </div>

                        <button 
                            onClick={() => router.push('/katalog/details?slug=' + a.name.replace(/ /g, "-") + '-' + a.id)}
                            type="button" 
                            className="btn mb-2 py-1 outline-btn lineup-btn">Lihat Detail</button>
                      </div>

                    </div>

                    
                  ))
                }                
              </ScrollMenu>
            </div>


          </div>

          <div className="col-12 text-center">
            <Link href="/katalog">
              <button type="text" className="btn mb-4 filled-btn align-self-center" style={{
                width: responsive == "mobile" ? "100%" : '20%',
              }}>Lihat Semua</button>
            </Link>
          </div>
        </div>
      </div>

      <div className="testi-container">
        <Image src="/bg-testi.png" width="100%" height="100%" layout="fill" objectFit="cover" />
        <div style={{ position: 'absolute', width: 'inherit', height: 'inherit', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
          <h1 className={"fw-bold color-white " + (responsive == 'mobile' ? "mb-3" : "mb-5")}>Eksplor Testimoni Klien Kami</h1>
          <div style={{ width: '90%' }}>
            <Slider {...settings} slidesToShow={testi.length >= 3 ? (responsive === "mobile" ? 1 : 3) : testi.length}>
              {
                testi?.map((testData, idx) => (
                  <div className="mb-4 mx-5 px-1" key={idx}>
                    <div className="d-flex align-items-center text-center flex-wrap py-3 white-card testi-card">
                      <div style={{ width: '28%' }} className="px-2 d-flex justify-content-center">
                        <img src={testData.avatar_source} style={{ width: "70%", borderRadius: 10 }} layout="fill" />
                      </div>
                      <div className="" style={{ width: '70%', textAlign: 'left' }}>
                        <p className="fs-5 fw-bold mb-1 lh-1">{testData.name}</p>
                        <div className="testi-content ">“{testData.testimony}”</div>
                      </div>
                    </div>

                  </div>
                ))
              }
            </Slider>
          </div>

        </div>
      </div>

      <div className="container-fluid  flex-wrap py-5 general-container">

        <div className="row justify-content-center benefit-container">
          <div className="col-12 benefit-container text-center">
            {/* <Slider {...settings} dots={true} arrows={true} slidesToShow={partner.length >= 6 ? ( responsive == "mobile" ? 5 : 6 ): partner.length}> */}
            <ScrollMenu Footer={<div></div>}
            LeftArrow={LeftArrow}
            RightArrow={RightArrow}
            onWheel={onWheel}>
              {
                partner?.map((part, idx) => (
                  <div className={" me-4"} key={idx}>
                    <a href={part.url_to} target="_blank">
                      <img src={part.logo_source} className="partner-img" />
                      {/* width: 120, height: 50 */}
                    </a>
                  </div>
                ))
              }
            </ScrollMenu>
            {/* </Slider> */}
          </div>
        </div>
      </div>

      <div className="container-fluid  flex-wrap py-5 general-container" style={{ backgroundImage: "url('/bg_lineup.png')", backgroundRepeat: 'no-repeat', backgroundSize: '100% 100%', backgroundAttachment: 'cover' }}>
        <div className="row justify-content-between align-items-center pb-5 benefit-container">
          <div className="col-12 ps-5 pb-5">
            <h1 className="fw-bold">Portofolio Kami</h1>
            <h4 className="color-secondary">Yuk Lihat Kebahagiaan Klien Kami</h4>
          </div>

          <div className="col-12 text-center pb-1">
            <div className="row justify-content-between align-items-center  benefit-container">
              <div className="col-xl-4 col-md-4 col-sm-12 mb-5 ps-5 pe-3 pb-2">
                <img src="/porto-home.png" style={{ width: responsive == "mobile" ? "80%" : '90%', height: responsive == "mobile" ? "80%" : '90%' }} />
              </div>

              <div className="col-xl-8 col-md-8 col-sm-12 mb-5 benefit-container pb-2">
                <Slider {...settings} slidesToShow={porto.length >= 3 ? (responsive === "mobile" ? 2 : 3) : porto.length}>
                  {
                    porto?.map((dataPorto, idx) => (
                      <div className={"col-3 mb-4 " + (responsive == "mobile" ? "px-1" : "px-3")} key={idx}>
                        <Link href={`/portofolio/details?slug=${dataPorto.slug}`}>
                          <div className="shadow-sm" style={{
                            backgroundColor: 'white',
                            borderRadius: 10
                          }}>
                            <img src={dataPorto.image_source} className="porto-img-home" />
                            <div className="px-3 py-2" style={{ height: 100 }}>
                              <div className="elipsis-2 fw-bold porto-title">{dataPorto.title}</div>
                              <div className="elipsis-2 porto-content">{dataPorto.subtitle}</div>
                            </div>

                          </div>
                        </Link>
                      </div>
                    ))
                  }
                </Slider>
              </div>
            </div>
          </div>
        </div>

      </div>
      <Footer />
    </div>
  )
}
