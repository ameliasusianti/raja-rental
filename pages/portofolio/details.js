import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'

import styles from '../../styles/Home.module.css'
import Api from '../../utils/Api'
import Navbar from '../../component/navbar'
import Footer from '../../component/footer'
import WaFloat from '../../component/wa-float'

export default function Home() {
    const [detail, setdetail] = useState({})

    const router = useRouter()
    const {slug} = router.query
    
    useEffect(() => {
        getPorto()
    }, [])

    const getPorto= async () => {
        try {
          let param = router.asPath?.split("?")?.[1]?.split("=")
          let fetch = await Api.get('/article/portfolio/' + param[1])
    
          if(fetch) {
            // console.log(fetch.data, 'porto')
            setdetail(fetch.data.response)
          }
        } catch (error) {
          
        }
      }

    let arr = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    return (
        <div className={styles.container}>
            <Navbar/>
            <WaFloat/>
            <div className="banner-screen">
                <Image src="/bg-porto-detail.png" width="100%" height="100%" layout="fill" objectFit="cover"/>
                <div style={{ position: 'absolute', width: 'inherit', height: 'inherit', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column', textAlign: 'center' }}>
                <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>{detail?.title}</h4>
                </div>
            </div>

            <div className="container-fluid  flex-wrap py-5 benefit-container" style={{ backgroundColor: '#F2F3F7' }}>
                <div className={"col-12 general-container " + (+window.innerWidth < 550 ? "pb-2" : "pb-3")}>
                    <h4 className="porto-detail-hist" style={{ color: '#F48C06' }}>Berita &gt; {detail?.title}</h4>
                    <h1 className="porto-detail-title" style={{ fontWeight: 600 }}>Project Kami</h1>
                </div>

                <div className="col-12 general-container pb-3 ">
                    <img src={detail?.banner_source} width="100%" height="100%" layout="fill" />
                    <h1 className={"pt-3 porto-detail-title " + (+window.innerWidth < 550 ? "mb-2" : "mb-3")}style={{ fontWeight: 600 }}>{detail?.title}</h1>
                    <div style={{ whiteSpace: 'pre-line'  }} dangerouslySetInnerHTML={{ __html: detail?.content }} ></div>
                </div>
            </div>
            
            <Footer/>
            
        </div>
    )
}