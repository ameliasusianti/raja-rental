import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router'
import Image from 'next/image'
import Link from 'next/link'

import styles from '../../styles/Home.module.css'
import Api from '../../utils/Api'
import Navbar from '../../component/navbar'
import Footer from '../../component/footer'
import WaFloat from '../../component/wa-float'



export default function Home() {
    const [porto, setporto] = useState([])
    const [total_page, settotal] = useState(1)
    const [responsive, setresp] = useState("")
    const [query, setquery] = useState({            
        pagination: true,
        page: 1,
        row: 8
    })

    const router = useRouter()
    
    useEffect(() => {
        getPorto(router.asPath)
        setresp(window.innerWidth < 500 ? "mobile" : (window.innerWidth <= 950 && "tablet"))
        const handleRouteChange = (url, { shallow }) => {
          getPorto(url)
        }
    
        router.events.on('routeChangeStart', handleRouteChange)
    
    }, [])

    const getPorto= async (url) => {
        let params = {...query}
        let queryTemp = {...query}
        let allParam = url?.split("?")?.[1]?.split("&")
        
        if(allParam) {
            for(let i = 0; i < allParam.length; i++) {
                let splitQuery = allParam[i].split("=")
                params[splitQuery[0]] = splitQuery[1]
                queryTemp[splitQuery[0]] = splitQuery[1]
            }
        }
        
        setquery(queryTemp)
        
        try {
          let fetch = await Api.get('/article/portfolio', {params})
    
          if(fetch) {
            settotal(Math.ceil(fetch.data.response.count/8))
            setporto(fetch.data.response.rows)
          }
        } catch (error) {
          
        }
      }

      const replaceQuery = (name, value) => {
        let params = {...query}
        params[name] = value
        let queryParams = ''
        for(let key in params) {
            queryParams = queryParams + `${queryParams.length > 0 ? '&' : ''}${key}=${params[key]}`
        }

        router.replace('/portofolio?' + queryParams)

    }

    return (
        <div className={styles.container}>
            <Navbar/>
            <WaFloat/>
            <div className="banner-screen">
                <Image src="/bg-laptop.png" width="100%" height="100%" layout="fill" objectFit="cover"/>
                <div style={{ position: 'absolute', width: 'inherit', height: 'inherit', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                    <h1 style={{ fontWeight: 700, color: 'white', marginBottom: 50 }}>PORTOFOLIO</h1>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Event Yang Sukses dan</h4>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Senyum Bahagia Customer Kami</h4>
                </div>
            </div>

            <div className="container-fluid  flex-wrap py-5 benefit-container" style={{ backgroundColor: '#F2F3F7' }}>
                <div className="col-12 general-container pb-5">
                    <h4 style={{ color: '#F48C06' }}>Portofolio</h4>
                    <h1 style={{ fontWeight: 600 }}>Inilah Project <span style={{ color: '#B01029' }}>Raja Rental Indonesia</span></h1>
                </div>

                <div className="col-12 general-container pb-3 ">
                    <div className="row">
                    {
                        porto?.map((data, idx) => (
                            <div className="col-xl-3 col-md-6 col-sm-12 col-12 mb-4" key={idx} style={responsive == "mobile" ? {paddingLeft: 6, paddingRight: 6} : {}}>
                                <Link href={`/portofolio/details?slug=${data.slug}`}>
                                        <div className="shadow-sm" style={{
                                            backgroundColor: 'white',
                                            borderRadius: 10
                                        }}> 
                                            <img src={data.image_source}
                                            class="porto-img"/>
                                            <div className="px-3 py-2" style={{height: 115}}>
                                                <div className="porto-title elipsis-2 fw-bold">{data.title}</div>
                                                <div className="elipsis-2 porto-subtitle">{data.subtitle}</div>
                                            </div>
                                            
                                        </div>
                                </Link>
                            </div>
                    ))}
                        
                    </div>
                </div>


                {
                    total_page > 1 && (
                        <div className="col-12 px-5 pb-3 ">
                            <div className="row justify-content-center">
                                <div className="col-3 d-flex justify-content-center">
                                    <nav aria-label="Page navigation example">
                                        <ul className="pagination">
                                            <li className={"page-item " + (query.page == 1 ? "disabled" : "")}>
                                                <a className="page-link" aria-label="Previous" onClick={() => replaceQuery('page', +query.page - 1)}>
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                            {
                                                query.page == 1 ?  null :(
                                                    <React.Fragment>
                                                        {
                                                            query.page == total_page &&
                                                            Number(query.page) - 1 !== 0 &&
                                                            total_page > 2 &&
                                                            <li className="page-item" onClick={() => replaceQuery('page', +query.page - 2)}><a className="page-link" href="">{Number(query.page)-2}</a></li>
                                                        }
                                                        
                                                        <li className="page-item" onClick={() => replaceQuery('page', +query.page - 1)}><a className="page-link" href="">{Number(query.page)-1}</a></li>
                                                    </React.Fragment>
                                                )
                                            }
                                            <li className="page-item active"><a className="page-link" href="#">{query.page}</a></li>
                                            {
                                                +query.page == total_page ?  null :(
                                                    <React.Fragment>
                                                        <li className="page-item" onClick={() => replaceQuery('page', +query.page + 1)}><a className="page-link" href="">{Number(query.page)+1}</a></li>
                                                        {
                                                            query.page == 1 &&
                                                            Number(query.page) + 1 !== total_page &&
                                                            <li className="page-item" onClick={() => replaceQuery('page', +query.page + 2)}><a className="page-link" href="">{Number(query.page)+2}</a></li>
                                                        }
                                                    </React.Fragment>
                                                )
                                            }
                                            <li className={"page-item " + (Number(query.page) === total_page ? "disabled" : "")}>
                                                <a className="page-link" aria-label="Next" onClick={() => replaceQuery('page', +query.page + 1)}>
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>
            
            <Footer/>
            
        </div>
    )
}