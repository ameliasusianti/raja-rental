import React, { useState, useEffect } from 'react';
import Image from 'next/image'
import Swal from 'sweetalert2'

import Api from '../utils/Api'
import styles from '../styles/Home.module.css'
import Navbar from '../component/navbar'
import Footer from '../component/footer'
import WaFloat from '../component/wa-float'

export default function Home() {
    const [Wa, setWa] = useState({})
    const [openEmail, setopen] = useState(false)
    const [name, setname] = useState("")
    const [company, setcompany] = useState("")
    const [phone_number, setphone_number] = useState("")
    const [email, setemail] = useState("")
    const [question, setquestion] = useState("")
    const [footerVal, setfooterVal] = useState([])

    useEffect(() => {
      getWa()
      getFooterVal()
    }, [])
  
    const getWa = async () => {
      try {
        let fetch = await Api.get('/whatsapp-contact')
  
        if(fetch) {
          console.log(fetch)
          setWa(fetch.data.response)
        }
      } catch (error) {
        
      }
    }

    const sendQuestion = async (e) => {
        e.preventDefault()
        try {
            console.log('here')
            if(
                name !== "" &&
                company !== "" &&
                phone_number !== "" &&
                email !== "" &&
                question !== "" ) {
                    let send = Api.post('/email-contact', {
                        name, company, phone_number, email, question
                    })

                    if(send) {
                        Swal.fire(
                'Success',
                'Your question has been send',
                'success'
              )
                    }
                } else {
                    Swal.fire(
                        'Warning',
                        'Please fill all the required field',
                        'warning'
                      )
                }
        } catch (error) {
            console.log(error)
        }
    }

    const getFooterVal = async () => {
        try {
          let fetch = await Api.get('/content-footer')
    
          if(fetch) {
            setfooterVal(fetch.data.response)
          }
        } catch (error) {
          
        }
      }

    return (
        <div className={styles.container}>
            <Navbar/>
            <WaFloat/>
            <div className="banner-screen">
                <Image src="/bg-kontak.png" width="100%" height="100%" layout="fill" objectFit="cover" />
                <div style={{ position: 'absolute', width: 'inherit', height: 'inherit', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                    <h1 style={{ fontWeight: 700, color: 'white', marginBottom: 50 }}>BUTUH BANTUAN LAIN</h1>
                    {/* {
                        window.innerWidth < 550 ? (
                            <h3 style={{ color: 'white', textAlign: 'center', width: '85%' }}>Jika Kamu Membutuhkan Bantuan Lain Silahkan Kontak Kami</h3>
                        ) : 
                        (
                            <React.Fragment>
                                <h3 style={{ color: 'white', textAlign: 'center' }}>Jika Kamu Membutuhkan Bantuan Lain</h3>
                                <h3 style={{ color: 'white', textAlign: 'center' }}>Silahkan Kontak Kami</h3>
                            </React.Fragment>
                        )
                    } */}
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Jika Kamu Membutuhkan Bantuan Lain</h4>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Silahkan Kontak Kami</h4>
                </div>
            </div>

            <div className="container-fluid  flex-wrap py-5 benefit-container" style={{ backgroundColor: '#F2F3F7' }}>
                <div className="col-12 general-container pb-3">
                    <h4 style={{ color: '#F48C06' }}>Kontak</h4>
                    <h1 style={{ fontWeight: 700 }}>Yuk Kontak <span style={{ color: '#B01029' }}>Raja Rental Indonesia</span></h1>
                </div>

                <div className={"col-12 " + (window.innerWidth > 500 ? "ps-5" : "")}>
                    <div className="row justify-content-center">
                        <div className="col-xl-8 col-md-8 col-sm-12 col-12 py-4">
                            <button type="button" 
                            onClick={() => window.open(Wa?.wa_url, "", "_blank")}
                            className="abt-us-btn btn d-flex align-items-center justify-content-center" 
                            style={{
                                backgroundColor: "#0CBC6A",
                                color: "#fff",
                            }}>
                                <Image src="/wa-white.png" width={'25vw'} height={'25vw'}/>
                                <span style={{marginLeft: '1rem'}}>Kontak Via Whatsapp</span></button>
                        </div>

                        <div className="col-xl-8 col-md-8 col-sm-10 col-10 d-flex align-items-center justify-content-center ">
                            <hr style={{width: '5rem', backgroundColor: 'black',borderWidth: 0, height: 2, margin: 0}}/>
                            <p style={{margin: '0 1.5rem', fontSize: '1.1rem'}}>atau</p>
                            <hr style={{width: '5rem', backgroundColor: 'black',borderWidth: 0, height: 2, margin: 0}}/>
                        </div>

                        <div className="col-xl-8 col-md-8 col-sm-12 col-12 py-4">
                            <button type="button" 
                            onClick={() => setopen(!openEmail)}
                            className="abt-us-btn btn d-flex align-items-center justify-content-center" 
                            style={{
                                backgroundColor: "#353535",
                                color: "#fff",
                            }}>
                                {
                                    window.innerWidth < 500 ? (
                                      <Image src="/mail-white.png" width={28} height={20}/>  
                                    ) : (
                                        <Image src="/mail-white.png" width={'32vw'} height={'22vw'}/>
                                    )
                                }
                                <span style={{marginLeft: '1rem'}}>Kontak Via Email</span></button>
                        </div>
                        
                        {
                            openEmail ? ( 
                            <React.Fragment>
                                <div className="col-10 col-xl-6 xol-md-6 col-sm-10 bg-white mb-4">
                                    <form>
                                        <div className="row px-4 py-5">
                                            <div className="col-12 col-xl-6 col-md-6 col-sm-12 mb-4">
                                                <input 
                                                type="text" 
                                                className="form-control" 
                                                placeholder="Nama"
                                                value={name}
                                                onChange={e => setname(e.target.value)}/>
                                            </div>
                                            <div className="col-12 col-xl-6 col-md-6 col-sm-12 mb-4">
                                                <input 
                                                type="text" 
                                                className="form-control" 
                                                placeholder="Perusahaan"
                                                value={company}
                                                onChange={e => setcompany(e.target.value)}/>
                                            </div>
                                            <div className="col-12 col-xl-6 col-md-6 col-sm-12 mb-4">
                                                <input 
                                                type="text" 
                                                className="form-control" 
                                                placeholder="No. Telp"
                                                value={phone_number}
                                                onChange={e => setphone_number(e.target.value)}/>
                                            </div>
                                            <div className="col-12 col-xl-6 col-md-6 col-sm-12 mb-4">
                                                <input 
                                                type="email" 
                                                className="form-control" 
                                                placeholder="Email"
                                                value={email}
                                                onChange={e => setemail(e.target.value)}/>
                                            </div>
                                            <div className="col-12 mb-4">
                                                <textarea 
                                                className="form-control" 
                                                placeholder="Pertanyaan" 
                                                style={{height: 150}}
                                                value={question}
                                                onChange={e => setquestion(e.target.value)}/>
                                            </div>
                                            <div className="col-12">
                                                <button 
                                                onClick={sendQuestion}
                                                type="button" 
                                                className="btn" style={{
                                                    backgroundColor: "#B01029",
                                                    color: "white",
                                                    width: '100%',
                                                    fontSize: '1.1rem'
                                                }}>Kirim</button>
                                            </div>
                                            
                                        </div>
                                    </form>
                                </div>
                                <div className={"col-10 col-xl-4 xol-md-4 col-sm-10 " + (window.innerWidth > 500 ? "ps-5" : "")} style={{color: '#353535'}}>
                                    <h1 style={{ fontWeight: 600, marginBottom: 15 }}>Layanan Kami</h1>

                                    <table>
                                        <tr>
                                            <td className='pt-1 pb-1 foot-img'><Image src="/loc-pin-black.png" width={window.innerWidth < 500 ? 50 : 16} height={window.innerWidth < 500 ? 70 : 22} /></td>
                                            <td className='pt-1 pb-1 foot-txt' style={{paddingLeft: 10}}>{footerVal?.[1]?.name === "contact_address" ? footerVal[1].value : ''}</td>
                                        </tr>
                                        <tr>
                                            <td className='pt-1 pb-1 foot-img'><Image src="/phone-black.png" width={15} height={18} /></td>
                                            <td className='pt-1 pb-1 foot-txt' style={{paddingLeft: 10}}>
                                            <a href={"https://wa.me/0" + footerVal?.[2]?.value + encodeURI("?text=Halo kak, saya ingin bertanya mengenai penyewaan barang di Raja Rental Indonesia")} target="_blank" style={{textDecoration: 'none', color: 'rgb(53, 53, 53)'}}>
                                                {footerVal?.[2]?.name === "contact_phone_1" ? footerVal[2].value : ''}
                                            </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className='pt-1 pb-1 foot-img'><Image src="/mail-black.png" width={25} height={19} /></td>
                                            <td className='pt-1 pb-1 foot-txt' style={{paddingLeft: 10}}>
                                                <a href={"mailto:" + footerVal?.[4]?.value} target="_blank" style={{textDecoration: 'none', color: 'rgb(53, 53, 53)'}}>
                                                    {footerVal?.[4]?.name ===  "contact_email" ? footerVal[4].value : ''}
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    <div className="map mt-2">
                                        <iframe src="https://www.google.com/maps/d/embed?mid=1x89XW6KtXXEE_3UG_Sk6J_qJl3ZZ3ME&ehbc=2E312F"></iframe>
                                    </div>
                                    {/* <h5><Image src="/loc-pin-black.png" width={15} height={18} /><span style={{ marginLeft: 15 }}>Jl. Karawaci Raya Blok R No. 39, Tangerang</span> </h5>
                                    <h5><Image src="/phone-black.png" width={15} height={18} /><span style={{ marginLeft: 15 }}>080989999123</span> </h5>
                                    <h5><Image src="/phone-black.png" width={15} height={18} /><span style={{ marginLeft: 15 }}>080989999123</span> </h5>
                                    <h5><Image src="/mail-black.png" width={20} height={15} /><span style={{ marginLeft: 15 }}>admin@rajarentalindonesia.com</span> </h5> */}

                                </div>

                            </React.Fragment>
                            ) : null
                        }
                        
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
}