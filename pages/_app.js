import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import 'bootstrap/dist/css/bootstrap.css'; // Add this line
import '../styles/globals.css'
import '../styles/_customTheme.scss'
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Swal from 'sweetalert2/dist/sweetalert2.js'
import 'sweetalert2/src/sweetalert2.scss'
import store from '../store';
import Script from 'next/script'

function MyApp({ Component, pageProps }) {
  useEffect(() => {
      import("bootstrap/dist/js/bootstrap");
  }, []);

  return (
    <Provider store={store.store}>
      <PersistGate loading={null} persistor={store.persistor}>
        <Head>
          <title>Raja Rental Indonesia</title>
          <meta name="description" content="PT. Raja Rental Indonesia merupakan pusat sewa peralatan elektronik terlengkap di Indonesia untuk perusahaan dan berbagai event. Kami menyediakan sewa mulai dari : Sewa Laptop, PC / Komputer, AIO (All In One), Monitor, PC Server, dan Printer." />
          <meta name="robots" content="index,follow"></meta>
          <meta name="googlebot" content="index,follow"></meta>
          <meta name="keywords" content="sewa,raja,rental,rajarental,rajarental.co.id,sewa laptop,sewa pc,sewa aio,sewa peralatan elektronik,sewa raja rental,pc,laptop,komputer,kamera,sewa peralatan pc,komputer,pc,rajanya rental,rental pc,rental komputer,rental laptop,lcd,proyektor,projector,televisi,penyewaan,sewa murah"></meta>
          <meta name="author" content="PT Raja Rental Indonesia"></meta>
          <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
          <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
          <link rel="preconnect" href="https://fonts.googleapis.com"/>
          <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin/>
          <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"/>
          
          <Script async src="https://www.googletagmanager.com/gtag/js?id=G-644GDWM4MY" />
          <Script>{`window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'G-644GDWM4MY');`}</Script>
        </Head>
        <Component {...pageProps} />
      </PersistGate>
    </Provider>
  )
}

export default MyApp
