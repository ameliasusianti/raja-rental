import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import Image from 'next/image'

import Api from '../utils/Api'
import styles from '../styles/Home.module.css'
import Navbar from '../component/navbar'
import Footer from '../component/footer'
import WaFloat from '../component/wa-float'

export default function Home() {
    const [content, setcontent] = useState([])

    useEffect(() => {
      getContent()
    }, [])
  
    const getContent = async () => {
      try {
        let fetch = await Api.get('/faq')
  
        if(fetch) {
          console.log(fetch)
          setcontent(fetch.data.response)
        }
      } catch (error) {
        
      }
    }
    return (
        <div className={styles.container}>
            <Navbar/>
            <WaFloat/>
            <div className="banner-screen">
                <Image src="/bg-faq.png" width="100%" height="100%" layout="fill" objectFit="cover"/>
                <div style={{ position: 'absolute', width: 'inherit', height: 'inherit', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                    <h1 style={{ fontWeight: 700, color: 'white', marginBottom: 50 }}>FAQ</h1>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Yuk Lihat Pertanyaan Yang Sering</h4>
                    <h4 style={{ color: 'white', textAlign: 'center', paddingLeft: 5, paddingRight: 5 }}>Diajukan Oleh Customer Kami</h4>
                </div>
            </div>

            <div className="container-fluid  flex-wrap py-5 benefit-container" style={{ backgroundColor: '#F2F3F7' }}>
                <div className="col-12 general-container pb-3">
                    <h4 style={{ color: '#F48C06' }}>Tentang Kami &gt; FAQ</h4>
                    <h1 style={{ fontWeight: 700 }}>FAQ</h1>

                    <div className="accordion mt-5" id="accordionExample">
                        {
                            content.map((data,idx) => (
                                 <div className="accordion-item mb-4" key={idx}>
                                    <h2 className="accordion-header" id={`heading-${idx}`}>
                                        <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target={`#collapse-${idx}`} aria-expanded="true" aria-controls={`collapse-${idx}`}>
                                            {data.question}
                                        </button>
                                    </h2>
                                    <div id={`collapse-${idx}`} className="accordion-collapse collapse" aria-labelledby={`heading-${idx}`} data-bs-parent="#accordionExample">
                                        <div className="accordion-body" style={{ whiteSpace: 'pre-line' }}>
                                            {data.answer}
                                        </div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>

                </div>
            </div>

            <Footer/>
        </div>
    )
}