import React, { useState, useEffect } from 'react';
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import Api from '../utils/Api'

export default function Home() {
    const [Wa, setWa] = useState({})

    useEffect(() => {
      getWa()
    }, [])
  
    const getWa = async () => {
      try {
        let fetch = await Api.get('/whatsapp-contact')
  
        if(fetch) {
          console.log(fetch)
          setWa(fetch.data.response)
        }
      } catch (error) {
        
      }
    }

    return (
        <div style={{
            position: 'fixed',
            bottom: 0,
            right: 0,
            width: 80, 
            height: 100, 
            zIndex: 1000,
            }}>
            <a href={Wa?.wa_url} target="_blank">
                <img src="/wa-round.png" style={{ 
                    width: 70, 
                    height: 70, 
                    bottom: 30, 
                    position: 'absolute'
                    }}/>
            </a>
        </div>
    )
}