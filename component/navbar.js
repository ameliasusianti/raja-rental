import React, { useState, useEffect } from 'react';
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import { useRouter } from 'next/router'

export default function Home() {
    const { asPath, push } = useRouter()
    const [search, setsearch] = useState('')
    const [navMenu, setnavMenu] = useState([
        {
            path: '/',
            type: 'text',
            value: 'Beranda'
        },
        {
            path: '/katalog',
            type: 'text',
            value: 'Katalog'
        },
        {
            path: '/portofolio',
            type: 'text',
            value: 'Portofolio'
        },
        {
            path: '/',
            type: 'image',
            value: '/logo.png'
        },
        {
            path: '/about-us /faq',
            type: 'dropdown',
            value: 'Tentang Kami',
            option: [
                {
                    path: '/about-us',
                    type: 'text',
                    value: 'Perusahaan'
                },
                {
                    path: '/faq',
                    type: 'text',
                    value: 'FAQ'
                },
            ]
        },
        {
            path: '/news',
            type: 'text',
            value: 'Blog'
        },
        {
            path: '/kontak',
            type: 'text',
            value: 'Kontak'
        },
    ])

    useEffect(() => {
        if(window.innerWidth < 1200) {
            setnavMenu([...navMenu.slice(0, 3), ...navMenu.slice(4, 7)])
        }

    }, [])

    // console.log(window.innerWidth)
    
    return (
        <nav className="navbar navbar-expand-xl navbar-light bg-light sticky-top ">
            <div className="container-fluid">
                <Link href={"/"} className="navbar-toggler">
                    <img className="navbar-toggler" src="/logo.png" style={{width: window.innerWidth > 550 ? '9vw' : 80, border: 0}}/>
                </Link>
                
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="nav justify-content-center align-items-center py-3" style={{width: '100%'}}>
                        {
                            navMenu.map(data => (
                                <React.Fragment>
                                    {
                                        data.type === 'dropdown' ? (
                                            <li className="nav-item dropdown">
                                                <a style={{paddingRight: '0px'}} className={"rri-dropdown nav-link dropdown-toggle " + (data.path.includes(asPath) && asPath !== '/' ? styles.navLinkCustomActv : styles.navLinkCustom)} data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">{data.value}</a>
                                                <ul className="dropdown-menu">
                                                    {
                                                        data.option.map(opt => (
                                                            <li>
                                                                <Link href={opt.path}>
                                                                    <a className={"dropdown-item " + (asPath === opt.path ? styles.navLinkCustomActv : styles.navLinkCustom)}>{opt.value}</a>
                                                                </Link>
                                                            </li>
                                                        ))
                                                    }
                                                </ul>
                                            </li>
                                        ) : (
                                            <li className="nav-item">
                                                <Link href={data.path}>
                                                    {
                                                        data.type === 'text' ? (
                                                            <a className={"nav-link " + (data.path == '/' && asPath == '/' ? styles.navLinkCustomActv : (asPath.includes(data.path) && data.path !== '/' ? styles.navLinkCustomActv : styles.navLinkCustom))}>{data.value}</a>
                                                        ) : (
                                                            <Image src={data.value} width={'65vw'} height={'55vw'} />
                                                        )
                                                    }
                                                </Link>
                                            </li>)
                                    }
                                </React.Fragment>
                                
                            ))
                        }

                        {
                            window.innerWidth > 992 ? (
                                <li className="nav-item dropdown" style={{
                                    position: 'absolute',
                                    zIndex: 100,
                                    right: '15%',
                                    top: '32%'
                                }}>
                                    <a className={"rri-dropdown nav-link dropdown-toggle "} data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">
                                        <Image src="/magnify_glass.png" width={25} height={25} />
                                    </a>
                                    <ul className="dropdown-menu px-2" >
                                        <li>
                                            <input 
                                            value={search}
                                            onChange={(e) => setsearch(e.target.value)}
                                            onKeyDown={event => {if(event.key === 'Enter'){push('/katalog?kw=' + search)}}}
                                            type="text" 
                                            className="form-control" 
                                            style={{width: 250}} 
                                            placeholder="Search"/>
                                        </li>
                                    </ul>
                                </li>
                            ) :(
                                <li className="nav-item ms-4">
                                    <input 
                                        value={search}
                                        onChange={(e) => setsearch(e.target.value)}
                                        onKeyDown={event => {if(event.key === 'Enter'){push('/katalog?kw=' + search)}}}
                                        type="text" 
                                        className="form-control" 
                                        style={{width: '95%'}} 
                                        placeholder="Search"/>
                                </li>
                            )
                        }

                        
                            
                        
                    </ul>
                </div>
            </div>
        </nav>

      
    )

}