import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import Api from '../utils/Api'

export default function Home() {
    const [footerVal, setfooterVal] = useState([])

    useEffect(() => {
        getFooterVal()
      }, [])
    
      const getFooterVal = async () => {
        try {
          let fetch = await Api.get('/content-footer')
    
          if(fetch) {
            setfooterVal(fetch.data.response)
          }
        } catch (error) {
          
        }
      }

    return (
        <footer className="container-fluid  flex-wrap py-5 benefit-container" style={{ backgroundColor: '#353535' }}>
        <div className="row justify-content-center" style={{ color: 'white', paddingLeft: 2, paddingRight: 2 }}>
          <div className={"col-xl-7 col-md-7 col-sm-12 " + (window.innerWidth < 550 ? "ps-1" : "")}>
            <Image src="/logo-text.png" alt="Logo" width={115} height={40} />
            <p className="elipsis-4" style={{ width: '80%', marginTop: 15, fontWeight: 'lighter' }}>{footerVal?.[0]?.name === "footer_description" ? footerVal[0].value : ''}</p>
            <h6><b style={{ marginTop: 15 }}>&copy; 2022 PT Raja Rental Indonesia</b></h6>
          </div>
          <div className={"col-xl-5 col-md-5 col-sm-12 " + (window.innerWidth < 550 ? "ps-1" : "")}>
            <h5 style={{ fontWeight: 700, marginBottom: 15, marginTop: 10 }}>Kontak Kami</h5>

            <table>
              <tr>
                <td className='pt-1 pb-1 foot-img'><Image src="/loc-pin-white.png" width={window.innerWidth < 500 ? 50 : 16} height={window.innerWidth < 500 ? 70 : 22} /></td>
                <td className='pt-1 pb-1 foot-txt' style={{paddingLeft: 10}}>{footerVal?.[1]?.name === "contact_address" ? footerVal[1].value : ''}</td>
              </tr>
              <tr>
                <td className='pt-1 pb-1 foot-img'><Image src="/phone-white.png" width={15} height={18} /></td>
                <td className='pt-1 pb-1 foot-txt' style={{paddingLeft: 10}}>
                  <a href={"https://wa.me/" + footerVal?.[2]?.value + encodeURI("?text=Halo kak, saya ingin bertanya mengenai penyewaan barang di Raja Rental Indonesia")} target="_blank" style={{color: 'white', textDecoration: 'none'}}>
                    +{footerVal?.[2]?.name === "contact_phone_1" ? footerVal[2].value : ''}
                  </a>
                </td>
              </tr>
              <tr>
                <td className='pt-1 pb-1 foot-img'><Image src="/mail-white.png" width={18} height={12} /></td>
                <td className='pt-1 pb-1 foot-txt' style={{paddingLeft: 10}}>
                  <a href={"mailto:" + footerVal?.[4]?.value} target="_blank" style={{color: 'white', textDecoration: 'none'}}>
                    {footerVal?.[4]?.name ===  "contact_email" ? footerVal[4].value : ''}
                  </a>
                </td>
              </tr>
            </table>

            <ul className="list-group list-group-horizontal" style={{ listStyle: 'none', marginTop: 20 }}>
              <li style={{ marginRight: 15 }}><a href={footerVal?.[5]?.name ===  "social_instagram" ? footerVal[5].value : ''} target="_blank"><Image src="/ig.png" width={30} height={30} /></a></li>
              <li style={{ marginRight: 15 }}><a href={footerVal?.[6]?.name ===  "social_facebook" ? footerVal[6].value : ''} target="_blank"><Image src="/fb.png" width={30} height={30} /></a></li>
              <li style={{ marginRight: 15 }}><a href={footerVal?.[7]?.name ===  "social_twitter" ? footerVal[7].value : ''} target="_blank"><Image src="/twitter.png" width={30} height={30} /></a></li>
            </ul>
          </div>
        </div>
      </footer>
    )
}